// ==UserScript==
// @name           More Info Hatebu
// @namespace      http://creazy.net/
// @description    Add More Information in Hatena Bookmark
// @include        http://b.hatena.ne.jp/entrylist*
// @include        http://b.hatena.ne.jp/hotentry*
// @include        http://b.hatena.ne.jp/bookmarklist*
// ==/UserScript==

(function() {

	var d  = document;

	function moreinfo() {
		var op = d.getElementsByTagName('H3')[0].parentNode;
		if ( op.getAttribute('class') != 'option' ) {
			op = op.parentNode;
		}
		var as = op.getElementsByTagName('A');
		//alert(as.length);

		for ( var i=0; i<as.length; i++ ) {
			if ( as[i].getAttribute('class') == 'bookmark' ) {
				if ( as[i].getAttribute('infoadded') ) continue;
				// add thumbnail
				//------------------------------------------------------------
				as[i].parentNode.parentNode.style.backgroundImage
					= 'url(http://capture.heartrails.com/small?'+as[i].getAttribute('href')+')';
				as[i].parentNode.parentNode.style.backgroundPosition
					= 'left top';
				as[i].parentNode.parentNode.style.backgroundRepeat
					= 'no-repeat';
				as[i].parentNode.parentNode.style.paddingLeft
					= '130px';
				as[i].parentNode.parentNode.style.minHeight
					= '90px';

				// marked
				as[i].setAttribute('infoadded','1');
			}
			else if ( as[i].innerHTML.match(/^.*users?$/) && as[i].getAttribute('href').match(/^\/entry\//) ) {
				if ( as[i].getAttribute('infoadded') ) continue;
				as[i].style.verticalAlign = 'middle';

				tg = as[i];
				while (1) {
					if ( tg.getAttribute('class') == 'entry-footer' ) {
						break;
					}
					else if ( tg.nodeName == 'DL' ) {
						ddElement        = d.createElement('dd');
						tg.appendChild(ddElement);
						tg = tg.childNodes[tg.childNodes.length-1];
						break;
					}
					tg = tg.parentNode;
				}

				// add Delicious counter
				//------------------------------------------------------------
				iElement        = d.createElement('img');
				iElement.src    = 'http://labs.creazy.net/sbm/delicious/numberimg/'+as[i].getAttribute('href').replace(/^\/entry\/(.*)$/,'$1');
				iElement.border = '0';
				iElement.style.verticalAlign = 'middle';

				aElement        = d.createElement('a');
				aElement.href   = 'http://labs.creazy.net/sbm/entry?service=delicious&url='+encodeURIComponent(as[i].getAttribute('href').replace(/^\/entry\/(.*)$/,'$1'));
				aElement.target = '_blank';
				aElement.appendChild(iElement);

				tg.appendChild(aElement);

				// add livedoor clip counter
				//------------------------------------------------------------
				iElement        = d.createElement('img');
				iElement.src    = 'http://image.clip.livedoor.com/counter/medium/'+as[i].getAttribute('href').replace(/^\/entry\/(.*)$/,'$1');
				iElement.border = '0';
				iElement.style.verticalAlign = 'middle';

				aElement        = d.createElement('A');
				aElement.href   = 'http://clip.livedoor.com/page/'+as[i].getAttribute('href').replace(/^\/entry\/(.*)$/,'$1');
				aElement.target = '_blank';
				aElement.appendChild(iElement);

				tg.appendChild(aElement);

				// marked
				as[i].setAttribute('infoadded','1');
			}
		}
	}

	// for Auto Pager
	var scrollHeight = d.documentElement.scrollHeight;
	d.addEventListener(
		"scroll",
		function(e){
			if(d.documentElement.scrollHeight - scrollHeight > 100){
				scrollHeight = d.documentElement.scrollHeight;
				moreinfo();
			}
		},
		false
	);

	moreinfo();

})();
