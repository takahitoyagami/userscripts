// ==UserScript==
// @name          Gmail MonoSpace Font
// @namespace     http://creazy.net/2008/07/gmail_monospace_font.html
// @description	  Use a monospace font in gmail.
// @include       http://mail.google.com/*
// @include       https://mail.google.com/*
// ==/UserScript==

(function () {
	var styles = "div.ArwC7c, textarea.iE5Yyc { font-family: monospace !important; font-size: 12px !important; }";
	var heads  = document.getElementsByTagName("head");
	if (heads.length > 0) {
		var node = document.createElement("style");
		node.type = "text/css";
		node.appendChild(document.createTextNode(styles));
		heads[0].appendChild(node); 
	}
}) ();
