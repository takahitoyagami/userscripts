// ==UserScript==
// @name           Ust DL
// @namespace      http://creazy.net/
// @description    Show Download Link for Ustream.
// @include        http://www.ustream.tv/recorded/*
// @version        2.0.2
// ==/UserScript==

// Bookmarklet usage
// javascript:(function(){var s=document.createElement('script');s.charset='UTF-8';s.src='http://userscripts.org/scripts/source/70394.user.js?d='+new Date().getTime();document.getElementsByTagName('head')[0].appendChild(s)})();

(function() {

var w = (typeof(unsafeWindow) != 'undefined') ? unsafeWindow : window;
var d = w.document;
var l = w.location;
var u = w.ustream;
var a = {};
var div = d.createElement('div');

//console.log(u.vars);

// find liveHttpUrl URL
if ( u.vars.liveHttpUrl ) {

    if ( u.vars.liveHttpUrl.match(/\.mp4$/) ) {
        a = d.createElement('a');
        a.style.display = 'inline-block';
        a.style.padding = '5px';
        a.style.margin  = '5px';
        a.style.border  = '1px solid #CCC';
        a.style.MozBorderRadius    = '5px';
        a.style.WebkitBorderRadius = '5px';
        a.setAttribute("href", u.vars.liveHttpUrl);
        a.setAttribute("target", '_blank');
        a.innerHTML = 'Download as MP4';
        div.appendChild(a);

        a = d.createElement('a');
        a.style.display = 'inline-block';
        a.style.padding = '5px';
        a.style.margin  = '5px';
        a.style.border  = '1px solid #CCC';
        a.style.MozBorderRadius    = '5px';
        a.style.WebkitBorderRadius = '5px';
        a.setAttribute("href", u.vars.liveHttpUrl.replace(/(mp4)$/,'flv'));
        a.setAttribute("target", '_blank');
        a.innerHTML = 'Download as FLV';
        div.appendChild(a);
    } else {
        a = d.createElement('a');
        a.style.display = 'inline-block';
        a.style.padding = '5px';
        a.style.margin  = '5px';
        a.style.border  = '1px solid #CCC';
        a.style.MozBorderRadius    = '5px';
        a.style.WebkitBorderRadius = '5px';
        a.setAttribute("href", u.vars.liveHttpUrl);
        a.setAttribute("target", '_blank');
        a.innerHTML = 'Download';
        div.appendChild(a);
    }

    d.getElementById('adaptvDiv').appendChild(div);
}
// not find liveHttpUrl, and guess URL
else {
    var cid  = '';
    //console.log(u.vars.videoPictureUrl);
    if ( u.vars.videoPictureUrl.match(/1_([0-9]+)_([0-9]+)_320x240_b_1:[0-9]\.jpg$/) ) {
        cid = RegExp.$1;
    }
    var vid  = d.getElementById('videoId').value;
    if ( l.href.match(/^http:\/\/www\.ustream\.tv\/recorded\/([0-9]+)/) ) {
        vid = RegExp.$1;
    }
    var file = '/'+vid.substr(0,vid.length-3)+"/"+vid+"/1_"+cid+"_"+vid+".flv";
    var urls = [];
    
    // old videos
    if ( vid.length < 7 ) {
        urls[0] = 'http://vod-storage1.ustream.tv/ustreamstorage/content/0/1/0'+file;
    }
    // recent videos
    else {
        var sub_dir = vid.substr(0,vid.length-6);
        urls[0] = 'http://ustream.vo.llnwd.net/pd1/0/1/'+sub_dir+file;
        urls[1] = 'http://ustream.vo.llnwd.net/pd2/0/1/'+sub_dir+file;
        urls[2] = 'http://ustream.vo.llnwd.net/pd3/0/1/'+sub_dir+file;
        urls[3] = 'http://ustream.vo.llnwd.net/pd4/0/1/'+sub_dir+file;
        urls[4] = 'http://ustream.vo.llnwd.net/pd5/0/1/'+sub_dir+file;
        urls[5] = 'http://ustream.vo.llnwd.net/pd6/0/1/'+sub_dir+file;
    }
    
    for ( var i=0; i<urls.length; i++ ) {
        if ( i > 0 ) {
            div.innerHTML += ' or ';
        }
    
        a = d.createElement('a');
        a.setAttribute("href", urls[i]);
        a.setAttribute("target", '_blank');
        a.innerHTML = 'Download'+(i+1);
        div.style.display = 'inline-block';
        div.style.padding = '5px';
        div.style.margin  = '5px';
        div.style.border  = '1px solid #CCC';
        div.style.MozBorderRadius    = '5px';
        div.style.WebkitBorderRadius = '5px';
        div.appendChild(a);
    }

    d.getElementById('adaptvDiv').appendChild(div);
}


})();