// ==UserScript==
// @name        LDR with SBM Count Images
// @namespace   http://creazy.net/
// @description Add SBM Count Images behind entry dates on livedoor Reader
// @include     http://reader.livedoor.com/reader/*
// @version     0.1
// ==/UserScript==

// based on http://d.hatena.ne.jp/tnx/20060716/1152998347
// based on http://tokyoenvious.xrea.jp/b/web/livedoor_reader_meets_hatebu.html
// based on http://la.ma.la/blog/diary_200610182325.htm
// based on http://la.ma.la/blog/diary_200703221812.htm

(function(){
	var w = (typeof unsafeWindow == 'undefined') ? window : unsafeWindow;
	var description = "\u306B\u30D6\u30C3\u30AF\u30DE\u30FC\u30AF\u3055\u308C\u3066\u3044\u308B\u4EF6\u6570\u3067\u3059";

	// hatena
	w.entry_widgets.add('hb_counter', function(feed, item){
		var link = item.link.replace(/#/g,'%23');
		return [
			'<a href="http://b.hatena.ne.jp/entry/', link, '">',
			'<img src="http://d.hatena.ne.jp/images/b_entry.gif" border=0 height=16><img style="border:0;margin-left:3px;" ',
			'src="http://b.hatena.ne.jp/entry/image/',link, '"></a>'
		].join('');
	}, '\u306F\u3066\u306A\u30D6\u30C3\u30AF\u30DE\u30FC\u30AF'+description);

	// livedoor clip
	w.entry_widgets.add('ldc_counter', function(feed, item){
		var link = item.link.replace(/#/g,'%23');
		return [
			'<a href="http://clip.livedoor.com/page/', link, '">',
			'<img src="http://parts.blog.livedoor.jp/img/cmn/clip_16_12_w.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://image.clip.livedoor.com/counter/', link, '"></a>'
		].join('');
	}, 'livedoor clip'+description);

	// Delicious
	w.entry_widgets.add('delicious_counter', function(feed, item){
		var link = item.link;
		return [
			'<a href="http://labs.creazy.net/sbm/entry?service=delicious&amp;url=', link, '">',
			'<img src="http://static.delicious.com/img/delicious.med.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://labs.creazy.net/sbm/delicious/textimg/', link, '"></a>'
		].join('');
	}, 'Delicious'+description);

	// Yahoo!ブックマーク
	w.entry_widgets.add('ybm_counter', function(feed, item){
		var link = item.link;
		return [
			'<a href="http://bookmarks.yahoo.co.jp/url?url=', encodeURIComponent(link), '">',
			'<img src="http://i.yimg.jp/images/sicons/ybm16.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://num.bookmarks.yahoo.co.jp/image/small/', link, '"></a>'
		].join('');
	}, 'Yahoo!\u30D6\u30C3\u30AF\u30DE\u30FC\u30AF'+description);

	// Buzzurl
	w.entry_widgets.add('buzzurl_counter', function(feed, item){
		var link = item.link;
		return [
			'<a href="http://buzzurl.jp/entry/', link, '">',
			'<img src="http://buzzurl.jp/static/image/api/icon/add_icon_mini_08.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://api.buzzurl.jp/api/counter/', link, '"></a>'
		].join('');
	}, 'Buzzurl'+description);

	// FC2ブックマーク
	w.entry_widgets.add('fc2_counter', function(feed, item){
		var link = item.link;
		return [
			'<a href="http://bookmark.fc2.com/search/detail?url=', encodeURIComponent(link), '">',
			'<img src="http://bookmark.fc2.com/img/add-16.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://bookmark.fc2.com/image/users/', link, '"></a>'
		].join('');
	}, 'FC2\u30D6\u30C3\u30AF\u30DE\u30FC\u30AF'+description);

	// @niftyクリップ
	w.entry_widgets.add('nifty_counter', function(feed, item){
		var link = item.link;
		return [
			'<a href="http://clip.nifty.com/entry?url=', encodeURIComponent(link), '">',
			'<img src="http://clip.nifty.com/images/addclip_icn.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://api.clip.nifty.com/api/v1/image/counter/', link, '"></a>'
		].join('');
	}, '@nifty\u30AF\u30EA\u30C3\u30D7'+description);

	// POOKMARK Airlines
	w.entry_widgets.add('pookmark_counter', function(feed, item){
		var link = item.link;
		return [
			'<a href="http://pookmark.jp/url/', link, '">',
			'<img src="http://pookmark.jp/images/add/add.gif" border=0 height=16><img style="border:0;margin-left:3px" ',
			'src="http://pookmark.jp/count/', link, '"></a>'
		].join('');
	}, 'POOKMARK Airlines'+description);

})();