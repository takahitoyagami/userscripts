// ==UserScript==
// @name           YouTube High-Definitionize
// @namespace      http://creazy.net/
// @description    Add Downloadable links that the highest quality format (HD or MP4) in Youtube Page
// @include        http://*.youtube.com/*
// @include        http://youtube.com/*
// ==/UserScript==

(function() {

    var d = document;
    var w = (typeof(unsafeWindow) != 'undefined') ? unsafeWindow : window;
    var l = location;
    var s = w.swfArgs;

    /**
     * Redirect Higher Quality URL
     */
    function redirect() {
        if ( s['fmt_map'] == '22/2000000/9/0/115' ) {
            l.href = l.href + '&fmt=22';
        } else {
            l.href = l.href + '&fmt=18';
        }
        exit;
    }
    
    /**
     * create XmlHttpRequest
     */
    function createXHR() {
        if ( w.ActiveXObject ) {
            try {
                return new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    return new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e2) {
                    return null;
                }
            }
        } else if ( w.XMLHttpRequest ) {
            return new XMLHttpRequest();
        } else {
            return null;
        }
    }

    /**
     * check URL
     */
    function checkURL(video_id) {
        var url
            = 'http://'+location.host+'/watch'
            + '?v='+video_id
            + '&fmt=22';
        var XHR = createXHR();
        XHR.open( 'GET', url, true );
        XHR.onreadystatechange = function() { 
            if (XHR.readyState==4) {
                if ( match = XHR.responseText.match(/var swfArgs = ({.*})/) ) {
                    json = eval('('+RegExp.$1+')');
                    var s = d.getElementsByTagName('strong');
                    for ( var i=0; i<s.length; i++ ) {
                        if ( !s[i].innerHTML && s[i].getAttribute('class') == json['video_id'] ) {
                            link = d.createElement('a');
                            link.style.color   = '#fff';
                            link.style.font    = 'bold 10px/10px Arial';
                            link.style.padding = '1px';
                            if ( json['fmt_map'] == '22/2000000/9/0/115' ) {
                                link.style.backgroundColor = '#f00';
                                link.href      = '/get_video?fmt=22&video_id='+json['video_id']+'&t='+json['t'];
                                link.innerHTML = 'HD';
                                s[i].nextSibling.href += "&fmt=22";
                            } else {
                                link.style.backgroundColor = '#666';
                                link.href      = '/get_video?fmt=18&video_id='+json['video_id']+'&t='+json['t'];
                                link.innerHTML = 'MP4';
                                s[i].nextSibling.href += "&fmt=18";
                            }
                            s[i].appendChild(link);
                        }
                    }
                }
            }
        }
        XHR.send('');
    }

    /**
     * Add HD or MP4 on each links in YouTube List Page
     */
    function visualize() {
        var a = d.getElementsByTagName('a');
        for ( var i=0; i<a.length; i++ ) {
            match = '';
            if ( a[i].innerHTML.indexOf('<img') > 0 ) continue; // Skip Image Link
            if ( a[i].getAttribute('vid') )           continue; // Skip checked Link
            if ( a[i].href.match(/#$/) )              continue; // Skip functional Link
            if ( a[i].href.match(/watch\?v=([a-zA-Z0-9_-]*)/) ) {
                match = RegExp.$1;
                a[i].setAttribute('vid',1);
                strong = d.createElement('strong');
                strong.setAttribute('class',match);
                a[i].parentNode.insertBefore(strong, a[i]);
            }
        }
        
        var s = d.getElementsByTagName('strong');
        
        var c = '';
        for ( var i=0; i<s.length; i++ ) {
            if ( !s[i].innerHTML && s[i].getAttribute('class') && c.indexOf(s[i].getAttribute('class')) < 0 ) {
                c += ' ' + s[i].getAttribute('class');
                checkURL( s[i].getAttribute('class') );
            }
        }
    }
    
    /**
     * Add Customized Embed Link
     */
    function showCopyLink() {
        if( s['fmt_map'].indexOf('22/2000000/9/0/115') >= 0 ) {
            i = 22;
            m = 'High Definition';
        } else {
            i = 18;
            m = 'for iPod';
        }
        url_command
            = 'prompt('
            + '\'Copy & Past the MP4('+m+') URL\','
            + 'document.getElementById(\'watch-url-field\').value+\'&fmt='+i+'\''
            + ');'
            + 'void(0);';
        embed_command
            = 'prompt('
            + '\'Copy & Past the MP4('+m+') tag\','
            + 'document.getElementById(\'embed_code\').value.replace(/(http:\\/\\/[a-zA-Z0_-]*\\.youtube\\.com\\/[-_.a-zA-Z0-9\\/?&=%]+)/g,\'$1&ap=%252526fmt%253D'+i+'\')'
            + ');'
            + 'void(0);';
        // Create Links Block
        d.getElementById('watch-url-div').innerHTML
            += '<div id="URL-YT-video" style="text-align:right;padding-right:30px;">'
            +  '<a href="javascript:'+url_command+'">[ Highest-Quality-URL ]</a>'
            +  '</div>';
        d.getElementById('watch-embed-div').innerHTML
            += '<div id="EMBED-YT-video" style="text-align:right;padding-right:30px;">'
            +  '<a href="javascript:'+embed_command+'">[ Highest-Quality-Embed ]</a>'
            +  '</div>';
    }
    
    /**
     * Add HD or MP4 Download Links
     */
    function showDownloadLinks() {
        var base_url = '/get_video?video_id='+s['video_id']+'&t='+s['t']+'&fmt=';
        var base_css = ' style="font:bold 14px/1.5 Arial"';
        var html     = '<div id="DL-YT-video"'+base_css+'>';
        html += '<div id="DL-YT-video-formats-18"><a href="'+base_url+'18">[OK] Download MP4(iPod, fmt=18)</a></div>';
        if( s['fmt_map'].indexOf('22/2000000/9/0/115') >= 0 ) {
            html += '<div id="DL-YT-video-formats-22"><a href="'+base_url+'22">[OK] Download MP4(HD, fmt=22)</a></div>';
        } else {
            html += '<div id="DL-YT-video-formats-22">[NG] Download MP4(HD, fmt=22)</div>';
        }
        html += '</div>';
        // Create Links Block
        d.getElementById('watch-embed-div').innerHTML += html;
    }
    
    /**
     * Controller
     */
    if ( l.host.match(/[a-zA-Z\.]*youtube\.com$/) ) {
        if ( l.pathname.match(/\/watch/) ) {
            // always watch in Highest Quality
            if ( !l.search.match(/fmt=[0-9]*/) ) {
                redirect();
            }
            
            showCopyLink();
            showDownloadLinks();
        }
        // for Auto Pager
        var scrollHeight = d.documentElement.scrollHeight;
        d.addEventListener(
            "scroll",
            function(e){
                if(d.documentElement.scrollHeight - scrollHeight > 100){
                    scrollHeight = d.documentElement.scrollHeight;
                    visualize();
                }
            },
            false
        );
        visualize();
    }

})();