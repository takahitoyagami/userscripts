// ==UserScript==
// @name           Amazon Quick Search Review (JP)
// @namespace      http://creazy.net/
// @description    Display Search Results of the Book Review from Google Ajax Search API
// @include        http://www.amazon.co.jp/*
// @version        1.0.0
// ==/UserScript==

(function() {

var w = (typeof(unsafeWindow) != 'undefined') ? unsafeWindow : window;
var l = location;

/**
 * Main Class
 */
var AmazonQuickSearchReview = function () {

    //------------------------------------------------------------
    //------------------------------------------------------------

    /**
     * Add cookie
     */
    this.addCookie = function(key, value) {
        if ( !key || !value ) {
            return false;
        }
        document.cookie
            = key + '=' + escape(value) + '; '
            + 'expires=Tue, 1-Jan-2030 00:00:00 GMT; '
            + 'path=/; ';
    };
    /**
     * Get cookie
     */
    this.getCookie = function(key) {
        var cookies = document.cookie.split(';');
        for ( var i=0; i<cookies.length; i++ ) {
            if ( cookies[i].indexOf('=') < 0 ) continue;
            key_value  = cookies[i].replace(/(^[\s]+|;)/g,'');
            deli_index = key_value.indexOf('=');
            if ( key_value.substring(0,deli_index) == key ) {
                return unescape(key_value.substring(deli_index+1,key_value.length));
            }
        }
        return '';
    };

    /**
     * escape HTML specialchars
     */
    this.esc = function(str) {
        str = '' + str;
        str = str.replace(/&/g,'&amp;');   // &
        str = str.replace(/</g,'&lt;');    // <
        str = str.replace(/>/g,'&gt;');    // >
        str = str.replace(/\"/g,'&quot;'); // "
        str = str.replace(/\'/g,'&#039;'); // '
        return str;
    };

    /**
     * Initialize
     */
    this.init = function() {
        // Affiliate values
        var asin   = document.getElementById('ASIN').value;
        var title  = document.getElementById('btAsinTitle').firstChild.textContent || document.getElementById('btAsinTitle').innerText;
        var q      = encodeURIComponent(title+' (書評 OR レビュー OR レポート) -site:amazon.co.jp');
        // http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=8&callback=aqsr.buildHTML&q=WEB%2BDB%20PRESS%20Vol.57%20%20-site%3Aamazon.co.jp
        var api    = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=8&callback=aqsr.buildHTML&q='+q;
        //var api    = 'http://ajax.googleapis.com/ajax/services/search/blogs?v=1.0&rsz=8&callback=aqsr.buildHTML&q='+q;
        var script  = document.createElement('script');
        script.type = 'text/javascript';
        script.src  = api;
        document.getElementsByTagName('head')[0].appendChild(script);
        //this.buildHTML();
    };

    /**
     * Build HTML
     */
    this.buildHTML = function(json) {
        console.log(json);
        //customerReviews
        var divtag = document.createElement('div');
        divtag.setAttribute('id','searchReviews');
        divtag.innerHTML = '<h2>【Googleからの書評検索】</h2>';
        if ( json.responseStatus != 200 ) {
            divtag.innerHTML += '<p>Google検索結果の取得に失敗しました。</p>';
        }
        else if ( json.responseData.results.length == 0 ) {
            divtag.innerHTML += '<p>Google検索結果が0件でした。</p>';
        }
        else {
            var result;
            var html
                = '<p>'+json.responseData.cursor.estimatedResultCount+'件の検索結果</p>'
                + '<ul>';
            for ( var i=0; i<json.responseData.results.length; i++ ) {
                result = json.responseData.results[i];
                html
                    += '<li>'
                    +  '<h3><a href="'+result.unescapedUrl+'">'+result.titleNoFormatting+'</a></h3>'
                    +  ((result.visibleUrl)?'<span>'+result.visibleUrl+'</span><br />':'')
                    +  '<p>'+result.content+'</p>'
                    +  '</li>';
            }
            html += '</ul>';
            divtag.innerHTML += html;
        }
        
        document.getElementById('customerReviews').insertBefore(
            divtag,
            document.getElementById('customerReviews').firstChild
        );
    };
};

/* add Class to global */
var script = document.createElement('script');
script.type = 'text/javascript';
script.text
    = 'var AmazonQuickSearchReview = ' + AmazonQuickSearchReview.toString() + ';'
    + 'var aqsr = new AmazonQuickSearchReview();'
document.body.appendChild(script);

/**
 * Crossbrowser addEventListener
 */
function _AddEventListener(e, type, fn) {
    if (e.addEventListener) {
        e.addEventListener(type, fn, false);
    }
    else {
        e.attachEvent('on' + type, function() {
            fn(window.event);
        });
    }
}

// Do it! (only in product page)
var aqsr = new AmazonQuickSearchReview();
var bodys = document.getElementsByTagName('body');
if ( bodys[0].getAttribute('class') == 'dp' ) {
    var navCatSubnav = document.getElementById('navCatSubnav');
    // 本カテゴリーの場合のみ起動
    if ( navCatSubnav ) {
        navCatSubnav_tds = navCatSubnav.getElementsByTagName('td');
        for ( var i=0; i<navCatSubnav_tds.length; i++ ) {
            if ( navCatSubnav_tds[i].getAttribute('class') == 'navCat' ) {
                if ( navCatSubnav_tds[i].getElementsByTagName('a')[0].innerHTML == '本' ) {
                    aqsr.init();
                }
            }
        }
    }
    // Can't Find Navibar
    else {
        aqsr.init();
    }
}

})();