// ==UserScript==
// @author         yager
// @email          yager@creazy.net
// @name           Hatebu Counter in Google Analytics
// @namespace      http://creazy.net/
// @description    Add hatebu counter images in Analytics tables
// @include        https://www.google.com/analytics/reporting/referring_sources*
// @version        1.0.0
// ==/UserScript==

// トラフィック：参照サイト
// https://www.google.com/analytics/reporting/referring_sources?id=1268224&pdr=20080903-20080903&cmp=average&trows=50
// コンテンツ：上位のコンテンツ
// https://www.google.com/analytics/reporting/top_content?id=1268224&bmid=7042216&pdr=20080903-20080903&cmp=average&trows=50
// コンテンツ：閲覧開始ページ
// https://www.google.com/analytics/reporting/entrances?id=1268224&bmid=7042216&pdr=20080903-20080903&cmp=average&trows=50
// コンテンツ：離脱ページ
// https://www.google.com/analytics/reporting/exits?id=1268224&bmid=7042216&pdr=20080903-20080903&cmp=average&trows=50

(function() {

    function addEventListener(e, type, fn) {
        if (e.addEventListener) {
            e.addEventListener(type, fn, false);
        }
        else {
            e.attachEvent('on' + type, function() {
                fn(window.event);
            });
        }
    }

/*

<div class="pagination_controls"> 
	<span class="button_label">次へ移動:</span> 
	<input id="f_row_start" size="5" value="1" onchange="table._toggleRowStart(this.value)" type="text"> 
	<span class="button_label">行を表示:</span> 
	<select onchange="table._toggleRowShow(this.options[this.selectedIndex].value)">
		<option value="10">10</option> 
		<option value="25">25</option> 
		<option value="50" selected="selected">50</option> 
		<option value="100">100</option> 
		<option value="250">250</option> 
		<option value="500">500</option>
	</select> 
	<span class="button_label"> 54 件中 1 - 50 件目 </span> 
	<a href="#" onclick="table._togglePage(-1); return false;" class="button previous" title="前へ"> <b> <b> <b> 前へ </b> </b> </b> </a> 
	<a href="#" onclick="table._togglePage(1); return false;" class="button next" title="次へ"> <b> <b> <b> 次へ </b> </b> </b> </a> 
</div>

*/

	var divs = document.getElementsByTagName('div');
	alert(divs.length);

	for ( var i=0; i<divs.length; i++ ) {
		if ( divs[i].getAttribute('class') == 'pagination_controls' ) {
			for ( var j=0; j<divs[i].childNodes.length; j++ ) {
				if ( divs[i].childNodes[j].tagName == 'INPUT' || divs[i].childNodes[j].tagName == 'SELECT' ) {
					//alert(divs[i].childNodes[j].tagName);
					// DOM Event
					addEventListener(divs[i].childNodes[j], 'change', function(e) {
						alert('change');
					});
				}
			}
			break;
		}
	}


})();
