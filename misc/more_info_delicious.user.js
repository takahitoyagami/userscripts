// ==UserScript==
// @name           More Info Delicious
// @namespace      http://creazy.net/
// @description    Add More Information in Delicious
// @include        http://delicious.com/*
// ==/UserScript==

(function() {

	var d  = document;

	function moreinfo() {
		var as = d.getElementById('bookmarklist').getElementsByTagName('A');
		for ( var i=0; i<as.length; i++ ) {
			if ( as[i].getAttribute('class') == 'taggedlink' ) {
				if ( as[i].getAttribute('infoadded') ) continue;
				// add thumbnail
				//------------------------------------------------------------
				as[i].parentNode.parentNode.parentNode.style.backgroundImage
					= 'url(http://capture.heartrails.com/tiny?'+as[i].getAttribute('href')+')';
				as[i].parentNode.parentNode.parentNode.style.backgroundPosition
					= 'left top';
				as[i].parentNode.parentNode.parentNode.style.backgroundRepeat
					= 'no-repeat';
				as[i].parentNode.parentNode.parentNode.style.paddingLeft
					= '70px';

				// open link new window
				//------------------------------------------------------------
				as[i].setAttribute('target','_blank');

				// add hatebu counter
				//------------------------------------------------------------
				iElement        = d.createElement('img');
				iElement.src    = 'http://b.hatena.ne.jp/entry/image/small/'+as[i].getAttribute('href');
				iElement.border = '0';

				aElement        = d.createElement('A');
				aElement.href   = 'http://b.hatena.ne.jp/entry/'+as[i].getAttribute('href');
				aElement.target = '_blank';
				aElement.appendChild(iElement);

				as[i].parentNode.appendChild(aElement);

				// add livedoor clip counter
				//------------------------------------------------------------
				iElement        = d.createElement('img');
				iElement.src    = 'http://image.clip.livedoor.com/counter/'+as[i].getAttribute('href');
				iElement.border = '0';

				aElement        = d.createElement('A');
				aElement.href   = 'http://clip.livedoor.com/page/'+as[i].getAttribute('href');
				aElement.target = '_blank';
				aElement.appendChild(iElement);

				as[i].parentNode.appendChild(aElement);

				// marked
				as[i].setAttribute('infoadded','1');
			}
		}
	}

	// for Auto Pager
	var scrollHeight = d.documentElement.scrollHeight;
	d.addEventListener(
		"scroll",
		function(e){
			if(d.documentElement.scrollHeight - scrollHeight > 100){
				scrollHeight = d.documentElement.scrollHeight;
				moreinfo();
			}
		},
		false
	);

	moreinfo();

})();
