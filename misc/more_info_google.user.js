// ==UserScript==
// @name           More Info Google
// @namespace      http://creazy.net/
// @description    Add More Information in Google Search
// @include        http://www.google.*/search*
// ==/UserScript==

(function() {

	var d  = document;

	function moreinfo() {
		var ols = d.getElementsByTagName('ol');
		for ( var i=0; i<ols.length; i++ ) {
			if ( ols[i].getAttribute('infoadded') ) continue;
			as  = ols[i].getElementsByTagName('A');
			for ( var j=0; j<as.length; j++ ) {
				if ( as[j].getAttribute('class') == 'l' ) {
					// add thumbnail
					//------------------------------------------------------------
					as[j].parentNode.parentNode.style.backgroundImage
						= 'url(http://capture.heartrails.com/small?'+as[j].getAttribute('href')+')';
					as[j].parentNode.parentNode.style.backgroundPosition
						= 'left top';
					as[j].parentNode.parentNode.style.backgroundRepeat
						= 'no-repeat';
					as[j].parentNode.parentNode.style.paddingLeft
						= '130px';
					as[i].parentNode.parentNode.style.minHeight
						= '100px';

					// open link new window
					//------------------------------------------------------------
					as[j].setAttribute('target','_blank');

					// add hatebu counter
					//------------------------------------------------------------
					brElement = d.createElement('br');
					as[j].parentNode.appendChild(brElement);

					iElement        = d.createElement('img');
					iElement.src    = 'http://b.hatena.ne.jp/entry/image/large/'+as[j].getAttribute('href');
					iElement.border = '0';

					aElement        = d.createElement('a');
					aElement.href   = 'http://b.hatena.ne.jp/entry/'+as[j].getAttribute('href');
					aElement.target = '_blank';
					aElement.appendChild(iElement);

					as[j].parentNode.appendChild(aElement);

					// add Delicious counter
					//------------------------------------------------------------
					iElement        = d.createElement('img');
					iElement.src    = 'http://labs.creazy.net/sbm/delicious/numberimg/'+as[j].getAttribute('href');
					iElement.border = '0';

					aElement        = d.createElement('a');
					aElement.href   = 'http://labs.creazy.net/sbm/entry?service=delicious&url='+as[j].getAttribute('href');
					aElement.target = '_blank';
					aElement.appendChild(iElement);

					as[j].parentNode.appendChild(aElement);

					// add livedoor clip counter
					//------------------------------------------------------------
					iElement        = d.createElement('img');
					iElement.src    = 'http://image.clip.livedoor.com/counter/medium/'+as[j].getAttribute('href');
					iElement.border = '0';

					aElement        = d.createElement('a');
					aElement.href   = 'http://clip.livedoor.com/page/'+as[j].getAttribute('href');
					aElement.target = '_blank';
					aElement.appendChild(iElement);

					as[j].parentNode.appendChild(aElement);
				}
			}
			// marked
			ols[i].setAttribute('infoadded','1');
		}
	}

	// for Auto Pager
	var scrollHeight = d.documentElement.scrollHeight;
	d.addEventListener(
		"scroll",
		function(e){
			if(d.documentElement.scrollHeight - scrollHeight > 100){
				scrollHeight = d.documentElement.scrollHeight;
				moreinfo();
			}
		},
		false
	);

	moreinfo();

})();
