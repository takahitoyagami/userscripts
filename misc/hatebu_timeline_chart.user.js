// ==UserScript==
// @author         yager
// @email          yager@creazy.net
// @name           Timeline Chart in Hatena Bookmark Entry
// @namespace      http://creazy.net/
// @description    Render Bookmark timeline chart in hatena bookmark entry.
// @include        http://b.hatena.ne.jp/entry/*
// @version        1.0.1 Fixed bug: changed concat date text as Strings.
//                 1.0.0 first released
// ==/UserScript==

(function() {

    // Request Hatena Bookmark Entry JSON API
    requestChartAPI();

    function requestChartAPI() {
		GM_xmlhttpRequest({
			method: 'GET',
			url: 'http://b.hatena.ne.jp/entry?mode=json&url='+encodeURIComponent(document.getElementById('entrylink_url').firstChild.href),
			onload: function(xhr) {
				var chartInfo = eval('(' + xhr.responseText + ')');
				renderChart(chartInfo);
			}
		});
    }

    function renderChart(chartInfo) {
		var today = new Date();
		var thisyy = today.getYear();  if (thisyy < 2000) { thisyy += 1900; }
		var thismm = today.getMonth();
		var thisdd = today.getDate();
		var thishh = today.getHours();

		var daily      = [];
		var dailyLabel = [];
		for ( var i=0; i<24; i++ ) {
			dt = new Date(today.getTime()-(i*60*60*1000));
			yy = dt.getYear();      if (yy < 2000) { yy += 1900; }
			mm = dt.getMonth() + 1; if (mm < 10) { mm = "0" + mm; }
			dd = dt.getDate();      if (dd < 10) { dd = "0" + dd; }
			hh = dt.getHours();     if (hh < 10) { hh = "0" + hh; }
			daily[i]      = 0;
			dailyLabel[i] = yy+""+mm+""+dd+""+hh;
		}

		var monthly      = [];
		var monthlyLabel = [];
		for ( var i=0; i<31; i++ ) {
			dt = new Date(today.getTime()-(i*24*60*60*1000));
			yy = dt.getYear();      if (yy < 2000) { yy += 1900; }
			mm = dt.getMonth() + 1; if (mm < 10) { mm = "0" + mm; }
			dd = dt.getDate();      if (dd < 10) { dd = "0" + dd; }
			hh = dt.getHours();     if (hh < 10) { hh = "0" + hh; }
			monthly[i]      = 0;
			monthlyLabel[i] = yy+""+mm+""+dd;
		}

		var yearly      = [];
		var yearlyLabel = [];
		for ( var i=0; i<12; i++ ) {
			dt = new Date(thisyy,thismm-i,thisdd);
			yy = dt.getYear();
			mm = dt.getMonth() + 1;
			if (yy < 2000) { yy = yy + 1900; }
			if (mm < 10)   { mm = "0" + mm; }
			yearly[i]      = 0;
			yearlyLabel[i] = yy+""+mm;
		}

		var day_sum  = [0,0,0,0,0,0,0];
		var dayLabel = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

		var hour_sum  = [];
		var hourLabel = [];
		for ( var i=0; i<24; i++ ) {
			hour_sum[i] = 0;
			hourLabel[i] = i;
		}

		for ( var i=0; i<chartInfo.bookmarks.length; i++ ) {
			bookmark = chartInfo.bookmarks[i];
			document.getElementById('bookmark-user-'+bookmark.user).firstChild.innerHTML = bookmark.timestamp;

			// daily
			dt = bookmark.timestamp.replace(/(\d{4})\/(\d{2})\/(\d{2})\s(\d{2}).*/,"$1$2$3$4");
			if ( dailyLabel[dailyLabel.length-1] <= dt ) {
				//alert(dt);
				for ( var j=0; j<dailyLabel.length; j++ ) {
					if ( dailyLabel[j] == dt ) {
						daily[j]++;
						break;
					}
				}
			}

			// monthly
			dt = bookmark.timestamp.replace(/(\d{4})\/(\d{2})\/(\d{2}).*/,"$1$2$3");
			if ( monthlyLabel[monthlyLabel.length-1] <= dt ) {
				//alert(dt);
				for ( var j=0; j<monthlyLabel.length; j++ ) {
					if ( monthlyLabel[j] == dt ) {
						monthly[j]++;
						break;
					}
				}
			}

			// yearly
			dt = bookmark.timestamp.replace(/^(\d{4})\/(\d{2}).*/,"$1$2");
			if ( yearlyLabel[yearlyLabel.length-1] <= dt ) {
				//alert(dt);
				for ( var j=0; j<yearlyLabel.length; j++ ) {
					if ( yearlyLabel[j] == dt ) {
						yearly[j]++;
						break;
					}
				}
			}

			// day summary
			dt = bookmark.timestamp.substring(0,10).split('/');
			td = new Date(dt[0],dt[1]-1,dt[2]);
			day_sum[td.getDay()]++;

			// hour summary
			dt = bookmark.timestamp.replace(/\d{4}\/\d{2}\/\d{2}\s(\d{2}).*/,"$1");
			hour_sum[eval(dt)]++;
		}

		var daily_max  = Math.max.apply(null, daily);
		var daily_data = [];
		for ( var i=0; i<daily.length; i++ ) {
			daily_data[i] = (daily[i] / daily_max * 100);
			dailyLabel[i] = (dailyLabel[i]).substring(8,10);
		}
		daily_url = _makeChartUrl('last day',daily_data,dailyLabel,daily_max);

		var monthly_max  = Math.max.apply(null, monthly);
		var monthly_data = [];
		for ( var i=0; i<monthly.length; i++ ) {
			monthly_data[i] = (monthly[i] / monthly_max * 100);
			monthlyLabel[i] = (monthlyLabel[i]).substring(6,8);
		}
		monthly_url = _makeChartUrl('last month',monthly_data,monthlyLabel,monthly_max);

		var yearly_max  = Math.max.apply(null, yearly);
		var yearly_data = [];
		for ( var i=0; i<yearly.length; i++ ) {
			yearly_data[i] = (yearly[i] / yearly_max * 100);
			yearlyLabel[i] = (yearlyLabel[i]).substring(4,6);
		}
		yearly_url = _makeChartUrl('last year',yearly_data,yearlyLabel,yearly_max);

		var day_max  = Math.max.apply(null, day_sum);
		var day_data = [];
		for ( var i=0; i<day_sum.length; i++ ) {
			day_data[i] = (day_sum[i] / day_max * 100);
		}
		day_url = _makeSumChartUrl('Summary of Days',day_data,dayLabel,day_max);

		var hour_max  = Math.max.apply(null, hour_sum);
		var hour_data = [];
		for ( var i=0; i<hour_sum.length; i++ ) {
			hour_data[i] = (hour_sum[i] / hour_max * 100);
		}
		hour_url = _makeSumChartUrl('Summary of Hours',hour_data,hourLabel,hour_max);

		var chartElement = document.createElement('div');
		chartElement.innerHTML
			= '<img src="'+daily_url+'" /><img src="'+day_url+'" /><br />'
			+ '<img src="'+monthly_url+'" /><img src="'+hour_url+'" /><br />'
			+ '<img src="'+yearly_url+'" /><br />';

		document.getElementById('entry-infosub').appendChild(chartElement);

		// release
		daily = null;
		dailyLabel = null;
		daily_data = null;
		monthly = null;
		monthlyLabel = null;
		monthly_data = null;
		yearly = null;
		yearlyLabel = null;
		yearly_data = null;
		day_sum = null;
		dayLabel = null;
		day_data = null;
		hour_sum = null;
		hourLabel = null;
		hour_data = null;
	}

	function _makeChartUrl(ctitle,cdata,clabel,cmax) {
		url
			='http://chart.apis.google.com/chart'
			+'?chtt='+ctitle
			+'&chts=333333,12'
			+'&chs=400x100'
			+'&cht=lc'
			+'&chco=0077cc'
			+'&chm=B,e6f2fa,0,0,0'
			+'&chxs=0,333333,9,0|1,333333,9,0'
			+'&chxt=x,y'
			+'&chd=t:'+cdata.reverse().join(',')
			+'&chxl=0:|'+clabel.reverse().join('|')+'|1:|0|'+encodeURIComponent(cmax);
		return url;
	}

	function _makeSumChartUrl(ctitle,cdata,clabel,cmax) {
		url
			='http://chart.apis.google.com/chart'
			+'?chtt='+ctitle
			+'&chts=333333,12'
			+'&chs=400x100'
			+'&cht=lc'
			+'&chco=FF0000'
			+'&chm=B,FFEEEE,0,0,0'
			+'&chxs=0,333333,9,0|1,333333,9,0'
			+'&chxt=x,y'
			+'&chd=t:'+cdata.join(',')
			+'&chxl=0:|'+clabel.join('|')+'|1:|0|'+encodeURIComponent(cmax);
		return url;
	}

})();
