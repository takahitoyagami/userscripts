// ==UserScript==
// @name           Youtube Downloader (Bookmarklet)
// @namespace      http://creazy.net/
// @description    Add downloadable links in Youtube Page
// @include        http://*youtube.com/watch*
// ==/UserScript==

//Normal ：320x240  FLV
//&fmt=6 ：448x336  FLV High Quality
//&fmt=18：480x360  MP4 iPod
//&fmt=22：1280x720 MP4 HD

// Bookmarklet
// javascript:var w=window,d=document,s=d.createElement('script');s.charset='UTF-8';s.src='http://labs.creazy.net/greasemonkey/youtube_show_dl_bookmarklet.js';d.body.appendChild(s);void(0);



var d = document;
var h = '';
var s = swfArgs;
var u
    = 'http://'+location.host+'/get_video'
    + '?video_id='+s['video_id']
    + '&t='+s['t'];

// Formats [number, description]
var f = [
            [0, 'FLV:normal'],
            [6, 'FLV:HQ'],
            [18,'MP4:iPod'],
            [22,'MP4:HD']
        ];

if( !d.getElementById('DL-YT-video') && (location.href.match(/http:\/\/[a-zA-Z\.]*youtube\.com\/watch/)) ) {

    /**
     * no fmt
     */
    d.getElementById('watch-embed-div').innerHTML += '<div id="DL-YT-video"></div>';
    url = u;
    d.getElementById('DL-YT-video').innerHTML
        += '<div id="DL-YT-video-formats-'+f[0][0]+'">[checking] '+f[0][1]+'</div>';
    checkURL0 = function(json) {
        status = json.requests[json.requests.length-1].status;
        block  = document.getElementById('DL-YT-video-formats-'+f[0][0]);
        if ( status == 200 ) {
            block.innerHTML = '<a href="'+url+'">[OK] DL ('+f[0][1]+')</a><br />';
        }
        else {
            block.innerHTML = '[NG] DL ('+f[0][1]+')<br />';
        }
    }
    script = document.createElement('script');
    script.charset = 'UTF-8';
    script.src     = 'http://urlcheck.appspot.com/?callback=checkURL0&url='+encodeURIComponent( url );
    document.body.appendChild(script);

    /**
     * &fmt=6
     */
    d.getElementById('watch-embed-div').innerHTML += '<div id="DL-YT-video"></div>';
    url = u + (f[1][0]?'&fmt='+f[1][0]:'');
    d.getElementById('DL-YT-video').innerHTML
        += '<div id="DL-YT-video-formats-'+f[1][0]+'">[checking] '+f[1][1]+'</div>';
    checkURL1 = function(json) {
        status = json.requests[json.requests.length-1].status;
        block  = document.getElementById('DL-YT-video-formats-'+f[1][0]);
        // Exists
        if ( status == 200 ) {
            block.innerHTML = '<a href="'+url+'">[OK] DL ('+f[1][1]+')</a><br />';
        }
        // Not Exists
        else {
            block.innerHTML = '[NG] DL ('+f[1][1]+')<br />';
        }
    }
    
    script = document.createElement('script');
    script.charset = 'UTF-8';
    script.src     = 'http://urlcheck.appspot.com/?callback=checkURL1&url='+encodeURIComponent( url );
    document.body.appendChild(script);

    /**
     * &fmt=18
     */
    d.getElementById('watch-embed-div').innerHTML += '<div id="DL-YT-video"></div>';
    url = u + (f[2][0]?'&fmt='+f[2][0]:'');
    d.getElementById('DL-YT-video').innerHTML
        += '<div id="DL-YT-video-formats-'+f[2][0]+'">[checking] '+f[2][1]+'</div>';
    checkURL2 = function(json) {
        status = json.requests[json.requests.length-1].status;
        block  = document.getElementById('DL-YT-video-formats-'+f[2][0]);
        // Exists
        if ( status == 200 ) {
            block.innerHTML = '<a href="'+url+'">[OK] DL ('+f[2][1]+')</a><br />';
        }
        // Not Exists
        else {
            block.innerHTML = '[NG] DL ('+f[2][1]+')<br />';
        }
    }
    
    script = document.createElement('script');
    script.charset = 'UTF-8';
    script.src     = 'http://urlcheck.appspot.com/?callback=checkURL2&url='+encodeURIComponent( url );
    document.body.appendChild(script);

    /**
     * &fmt=22
     */
    d.getElementById('watch-embed-div').innerHTML += '<div id="DL-YT-video"></div>';
    url = u + (f[3][0]?'&fmt='+f[3][0]:'');
    d.getElementById('DL-YT-video').innerHTML
        += '<div id="DL-YT-video-formats-'+f[3][0]+'">[checking] '+f[3][1]+'</div>';
    checkURL3 = function(json) {
        status = json.requests[json.requests.length-1].status;
        block  = document.getElementById('DL-YT-video-formats-'+f[3][0]);
        // Exists
        if ( status == 200 ) {
            block.innerHTML = '<a href="'+url+'">[OK] DL ('+f[3][1]+')</a><br />';
        }
        // Not Exists
        else {
            block.innerHTML = '[NG] DL ('+f[3][1]+')<br />';
        }
    }
    
    script = document.createElement('script');
    script.charset = 'UTF-8';
    script.src     = 'http://urlcheck.appspot.com/?callback=checkURL3&url='+encodeURIComponent( url );
    document.body.appendChild(script);
}
