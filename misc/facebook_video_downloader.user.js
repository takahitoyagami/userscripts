// ==UserScript==
// @name           Facebook Video Downloader
// @namespace      http://creazy.net/
// @description    Download Facebook Videos
// @include        http://*.facebook.com/photo.php*
// @include        http://*.facebook.com/video/video.php*
// @version        1.0.0
// ==/UserScript==

(function() {

var console = {
  _defined: false,
  log: function(object) {
    if (!console._defined) {
      console._defined = true;
      location.href = "javascript:" + uneval(function() {
        document.addEventListener("consoleData",
        function(event) {
          console.log.apply(this, event.getData("object"));
        },
        false);
      }) + "()";
    }
    setTimeout(send, 100, arguments);
    function send(object) {
      var event = document.createEvent("DataContainerEvent");
      event.initEvent("consoleData", true, false);
      event.setData("object", object);
      document.dispatchEvent(event);
    }
  }
};

var w = (typeof(unsafeWindow) != 'undefined') ? unsafeWindow : window;
var d = w.document;
var l = w.location;

var FVD = function () {

    /**
     * create XmlHttpRequest
     */
    this.createXHR = function() {
        if ( window.ActiveXObject ) {
            try {
                return new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    return new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e2) {
                    return null;
                }
            }
        } else if ( window.XMLHttpRequest ) {
            return new XMLHttpRequest();
        } else {
            return null;
        }
    };
    /**
     * check Video
     */
    this.checkVideo = function() {
        var XHR = this.createXHR();
        XHR.open( 'GET', location.href, true );
        XHR.onreadystatechange = function() {
            if ( XHR.readyState==4 ) {
                //console.log(XHR.responseText);
                if ( matches = XHR.responseText.match(/addVariable\((\"[^\"]+?_src\",\s\"[^\"]+?\")/g) ) {
                    //console.log(matches);
                    json_text = '';
                    for ( i=0; i<matches.length; i++ ) {
                        console.log(matches[i]);
                        if ( i > 0 ) json_text += ',';
                        json_text += matches[i].replace("addVariable(","").replace(", ",":");
                    }
                    eval('var json = ({'+json_text+'})');
                    console.log( json );
                    console.log( decodeURIComponent(json.highqual_src) );
                    console.log( decodeURIComponent(json.lowqual_src) );
                }
            }
        }
        XHR.send('');
    };

}

/* add Class to global */
var script = document.createElement('script');
script.type = 'text/javascript';
script.text
    = 'var FVD = ' + FVD.toString() + ';'
    + 'var fvd = new FVD();'
d.body.appendChild(script);

/* add eventLister for cross browser */
function _AddEventListener(e, type, fn) {
    if (e.addEventListener) {
        e.addEventListener(type, fn, false);
    }
    else {
        e.attachEvent('on' + type, function() {
            fn(window.event);
        });
    }
}

/**
 * Controller
 */
var fvd = new FVD();
fvd.checkVideo();

})();
