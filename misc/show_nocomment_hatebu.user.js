var bookmarked_user = document.getElementById('bookmarked_user');
var bookmark_offset = 0;

function requestHatebuRSS() {
	var global = function() { return this }();
	global.__callbackHatebuRSS__ = renderHatebuRSS;
	pipes_url
		= 'http://pipes.yahoo.com/yager/hatebu_bookmarklist'
		+ '?_render=json'
		+ '&_callback=__callbackHatebuRSS__'
		+ '&url='+encodeURIComponent(unescape(location.href.substring(28)))
		+ '&of='+bookmark_offset;
	var s = document.createElement('script');
	s.src = pipes_url;
	document.body.appendChild(s);
}

function renderHatebuRSS(json)
{
	var items = json.value.items;
	for ( var i=0; i<items.length; i++ ) {
		item = items[i];
		if ( document.getElementById('bookmark-user-'+item['dc:creator']) ) continue;
		dc_date = item['dc:date'];
		dc_date = dc_date.replace("T"," ");
		icon_url = 'http://www.hatena.ne.jp/users/'+item['dc:creator'].substring(0,2)+'/'+item['dc:creator']+'/profile_s.gif';
		li=document.createElement('li');
		li.setAttribute('id','bookmark-user-'+item['dc:creator']);
		li_html
			= '<span class="timestamp">'+dc_date+'</span> '
			+ '<a href="/'+item['dc:creator']+'/" class="hatena-id-icon">'
			+ '<img src="'+icon_url+'" class="hatena-id-icon" alt="'+item['dc:creator']+'" title="'+item['dc:creator']+'" height="16" width="16"></a> '
			+ '<a href="'+item['rdf:about']+'" class="hatena-id">'+item['dc:creator']+'</a>';
		if ( item['dc:subject'] ) {
			if ( typeof(item['dc:subject']) == 'string' ) {
				item['dc:subject'] = new Array(item['dc:subject']);
			}
			tags = item['dc:subject'];
			li_html += '<span class="user-tag">';
			for ( var j=0; j<tags.length; j++ ) {
				li_html
					+= ' <a href="/'+item['dc:creator']+'/'+encodeURIComponent(tags[j])+'/" rel="tag" class="user-tag">'
					+  tags[j]+'</a>';
			}
			li_html += '</span>';
		}
		if ( item['description'] ) {
			li_html += '<span class="comment">'+item['description']+'</span>';
		}
		li.innerHTML += li_html;
		bookmarked_user.appendChild(li);
	}
	if ( items.length > 0 ) {
		bookmark_offset += items.length;
		requestHatebuRSS();
	} else {
		var s = document.createElement('script');
		s.innerHTML = "new Hatena.Star.EntryLoader();";
		document.body.appendChild(s);
	}
}

//if ( bookmarked_user.childNodes[1].textContent == 'ページ作者様の希望によりブックマークの一覧は非表示に設定されています。' ) {
	bookmarked_user.innerHTML = '';
	requestHatebuRSS();
//}
